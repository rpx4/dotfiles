# insert into .bashrc 
# if [ -f ~/.bash_aliases ]; then
#     . ~/.bash_aliases
# fi
# 

#This function will allow you to open files in vim and request a password if they are root files
function vim(){
  OWNER=$(stat -c '%U' $1)
  if [[ "$OWNER" == "root" ]]; then
    sudo -e /usr/bin/vim $*;    # note: [safer] sudo -e forces into edit only mode, does not allow executing cmds. [safest] sudo -k resets credentials forcing pw prompt each time. 
  else
    /usr/bin/vim $*;
  fi
}

#ip address | grep -Eo '192.168.1.[1-9]?[0-9]?[0-9]' | grep -v '192.168.1.255'

#ip address | grep -w inet | grep -v '127.0.0.1' | awk '{print $2}'| cut -d '/' -f 1

# ifconfig | grep -w inet | grep -v '127.0.0.1' | awk '{print $2}'

hostname | tr '\n' '\t'; ip -br -4 addr | cut -f1 -d/ | awk '{print $1, $3}' | grep -v '127'

mcd () { mkdir -p $1; cd $1; ls -l; }
me () { whoami; groups; }

ed() { command ed -sp": " "$@" ; }

# some aliases
# note: alias sudo added in so alias apt + command can be invoked under sudo without a bad command
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias cls=clear
alias ls='ls -hF --color=auto'
alias ll='ls -lF --color=auto'
alias lll='ls -laF --color=auto'
alias ls1='ls --group-directories-first'
alias lsd='ls -lhF --color=auto && du -sh0 && echo      Grand Total Size'
alias free='free -ht'
alias fr='free -ht'
alias today='date +%m/%d/%Y'
alias td='date +%m/%d/%Y'
alias mounted="mount | grep '^/' | sort | cut -f1 -d\("
alias grep='grep --color'
alias cll='cls;ls -l'
alias userlst='cut -f1 -d: /etc/passwd'
alias grouplst='cut -f1 -d: /etc/group'
alias dfls="df -Th | grep -v 'tmpfs' | sort"
alias uuidls='ls -l /dev/disk/by-uuid'
# alias ipconfig='hostname;hostname -I'
alias myip="hostname | tr '\n' '\t'; ip address | grep -Eo '192.168.1.[1-9]?[0-9]?[0-9]' | grep -v '192.168.1.255'"
# alias ipa="hostname | tr '\n' '\t'; ip address | grep -Eo '192.168.1.[1-9]?[0-9]?[0-9]' | grep -v '192.168.1.255'"
alias ipa="hostname | tr '\n' '\t'; ip -br -4 addr | cut -f1 -d/ | awk '{print \$1, \$3}' | grep -v '127'"
alias ipconfig='inxi -i'
# alias root='pkexec nautilus /'
alias root='pkexec nemo /'
# alias apt='eopkg'
# alias sudo='sudo '
alias about='inxi;inxi -S'
# alias df="df -Th | grep -v 'tmpfs'"
alias cd..='cd ..'
alias mi='micro'
alias ff="free | grep 'Total:' | (awk '{print $3}')"
# alias dfree="df -Th | sed -n -e '1p' -e '/^\/dev/p'"
alias dfree="df -Th | head -1; df -Th | grep ^'/dev' | sort"
alias edit='micro'
alias aping='arping -I eno1 -c 5'
alias deadbeef='/home/logan/myapps/deadbeef-1.8.2/deadbeef >/dev/null 2>&1 &'
alias mem="free | awk '/^Mem:/ {print \$3, \$2}'"
alias temp="sensors | awk '/^Core 0:/ {print \$3}'"
alias psmem="echo '-------- [Top memory hogs] --------';ps -axch -o cmd:15,pid:15,%mem --sort=-%mem | head -n 12"
alias pscpu="echo '---------- [Top cpu hogs] ----------';ps -axch -o cmd:15,pid:15,%cpu --sort=-%cpu  | head -n 12"
alias tophog="cls;psmem;pscpu"
alias calc="cls;echo ----[ calculator ]----;bc -q"
alias pf="ps -e | grep $1"
alias supered='sudo mousepad $1'
alias duhome='du -sh /home/* | sort -nr'
