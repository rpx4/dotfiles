"""" / VIM Colors / """"   <for color legend, see website: https://jonasjacek.github.io/colors>	or <https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg>	
"""colorscheme darkblue   					                " use this color scheme (built-in) (not being used) 
highlight Normal 	  ctermfg=246		ctermbg=234 			" set normal text to grey
highlight LineNr 	  ctermfg=61 		cterm=bold 			" set all line numbers to bold grey 
highlight CursorLineNr 	  ctermfg=81		cterm=bold   		        " set current line number to bold lightblue 
highlight IncSearch 	  ctermfg=yellow 					" set searches to be yellow 
highlight ModeMsg	  ctermfg=81 						" set mode messages to be lightblue (--INSERT-- ; --VISUAL--)
highlight Visual	  ctermfg=black 	ctermbg=81			" set visual mode when blocking text to black on lightblue 
highlight Statusline	  ctermfg=81 		ctermbg=black	 		" set Statusline to lightblue with black text showing the filename 
highlight Cursorline	  ctermfg=81      	ctermbg=25	cterm=bold	" set Cursorline to lightblue bold 


"""" / Statusline Colorbands / """" 						" / COLORS /
hi SLmode       ctermfg=233   ctermbg=81   cterm=bold 				" Mode you are in (--NORMAL-- --INSERT-- --VISUAL--) 
hi SLname       ctermfg=233   ctermbg=69  					" Filename with full path
hi SLcenter     ctermfg=233   ctermbg=25  					" Center of statusline
hi SLsave     	ctermfg=233   ctermbg=25 					" [+] if file was changed and needs saved
hi SLreadonly	ctermfg=233   ctermbg=25 					" [RO] if file is readonly
hi SLtype       ctermfg=233   ctermbg=69  					" Identify the filetype
hi SLpos        ctermfg=233   ctermbg=111  					" Line position 
hi SLper        ctermfg=233   ctermbg=75   					" Percentage of document 


"""" / Statusline Definitions / """"						" / DEFINITIONS /
set statusline+=%#SLmode#%{(mode()=='n')?'\ \ NORMAL\ ':''} 			" NORMAL mode
set statusline+=%#SLmode#%{(mode()=='i')?'\ \ INSERT\ ':''}			" INSERT mode
set statusline+=%#SLmode#%{(mode()=='r')?'\ \ REPLACE\ ':''} 			" REPLACE mode
set statusline+=%#SLmode#%{(mode()=='v')?'\ \ VISUAL\ ':''}			" VISUAL mode
"set statusline+=%#SLmode#%{(mode()=='V')?'\ \ V-BLOCK\ ':''}			" VISUAL mode
"set statusline+=%#SLmode#%{(mode()=='r')?'\ \ R\ ':''}				" VISUAL mode
"set statusline+=%#SLmode#%{(mode()=='Rv')?'\ \ V-REPLACE\ ':''}		" VISUAL mode

set statusline+=%#SLname#\ %F\    						" Full filename path	 %t = filename (tail) of file. 
set statusline+=%#SLcenter#%=							" Center of statusline  
set statusline+=%#SLreadonly#\%r\    				 		" [RO] if file is readonly	  
set statusline+=%#SLsave#\%m\    				 		" [+] if file is changed	  
set statusline+=%#SLtype#\ %Y\ 							" Identify the filetype
set statusline+=%#SLpos#\ %3l:%-2c\ 						" Line position 
set statusline+=%#SLper#\ %3p%%\ 						" Percentage of document


"""" / Basic Behavior / """"
syntax off              " turn off setfiletype highlights (text coloring based on filetypes)
set number              " show line numbers
set nowrap              " do not wrap lines
set cursorline          " highlight current line
set laststatus=2        " always show statusline (even with only single window)
set ruler               " show line and column number of the cursor on right side of statusline
set hlsearch		" Highlight all for searched term
set ignorecase		" ignore case
set smartcase		" smart uppercase
set undofile		" saves all changes to a file ending in filename.un~
set noshowmode          " do not show mode (--INSERT--, --VISUAL--, etc). Turn off due it to being added to SL instead 
set shortmess=F         " set to not show messages on the bottom on file (seen when vim is first opened a file)

"set nocompatible       " do not be compatible with original vi (don't need this if you have a .vimrc) 
"set backspace=2        " allow the backspace to function (don't need this if you have a .vimrc) 
"set background=dark    " set vim to dark background
"set autoread           " autoreload the file in Vim if it has been changed outside of Vim
"set wildmenu


"""" / Key Mappings / """"
map <Backspace> :noh<cr><c-l>			| " backspace : clears any previous search highlights 
map <c-x> <esc>:x<cr>				| " control-x : save and exit now (:x,:wq)
map <c-z> <esc>:q!<cr>				| " control-z : do not save just exit (:q!)
" map <c-e> <esc>:!vifm<cr> 			| " control-e : open vifm to browse for a file to edit 
map <c-l> <esc>:!clear;ls -Fah\|grep -v /<cr>
map 1 0						| " 1         : go to beginning of line
map 5 $						| " 5         : go to end of line
"map v V						| " v	      : Visual V-block line mode
map t gg					| " t	      : top
map b G						| " b	      : bottom
"cmap w!! w !sudo tee > /dev/null %
" note: map c-l will not work with a comment on the right.  " control-l : list files from current directory

